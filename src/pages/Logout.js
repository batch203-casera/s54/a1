import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import userContext from '../userContext';

export default function Logout() {
    const { unsetUser, setUser } = useContext(userContext);
    unsetUser();
    useEffect(() => {
        return () => {
            setUser({ email: null });
        }
    }, [])
    return (
        <Navigate to="/login" />
    )
}
